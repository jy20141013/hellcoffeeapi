package com.jy.hellcoffeeapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberInfoChangeRequest {
    private String phoneNumber;
    private String memo;
}
