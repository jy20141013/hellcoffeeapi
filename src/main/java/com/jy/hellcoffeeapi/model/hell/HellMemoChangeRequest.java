package com.jy.hellcoffeeapi.model.hell;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HellMemoChangeRequest {
    private String etcMemo;
    private String cafeName;
    private Integer payPrice;
}
