package com.jy.hellcoffeeapi.model.hell;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HellCreateRequest {
    private LocalDate dateHell;
    private Integer payPrice;
    private String cafeName;
    private String etcMemo;
}
