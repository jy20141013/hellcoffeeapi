package com.jy.hellcoffeeapi.model.hell;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HellItem {
    private Long id;
    private Long memberId;
    private String memberName;
    private LocalDate dateHell;
    private Integer payPrice;
}
