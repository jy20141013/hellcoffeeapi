package com.jy.hellcoffeeapi.service;

import com.jy.hellcoffeeapi.entity.Hell;
import com.jy.hellcoffeeapi.entity.Member;
import com.jy.hellcoffeeapi.model.hell.HellCreateRequest;
import com.jy.hellcoffeeapi.model.hell.HellItem;
import com.jy.hellcoffeeapi.model.hell.HellMemoChangeRequest;
import com.jy.hellcoffeeapi.repository.HellRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HellService {
    private final HellRepository hellRepository;

    public void setHell(Member member, HellCreateRequest request) {
        Hell addData = new Hell();
        addData.setMember(member);
        addData.setDateHell(request.getDateHell());
        addData.setPayPrice(request.getPayPrice());
        addData.setCafeName(request.getCafeName());
        addData.setEtcMemo(request.getEtcMemo());

        hellRepository.save(addData);
    }

    public List<HellItem> getHells() {
        List<Hell> originList = hellRepository.findAll();
        List<HellItem> result = new LinkedList<>();

        for( Hell hell : originList) {
            HellItem addItem = new HellItem();
            addItem.setId(hell.getId());
            addItem.setMemberId(hell.getMember().getId());
            addItem.setMemberName(hell.getMember().getName());
            addItem.setDateHell(hell.getDateHell());
            addItem.setPayPrice(hell.getPayPrice());

            result.add(addItem);
        }

        return result;
    }

    public void putHell(long id, HellMemoChangeRequest request) {
        Hell originData = hellRepository.findById(id).orElseThrow();
        originData.setPayPrice(request.getPayPrice());
        originData.setEtcMemo(request.getEtcMemo());
        originData.setCafeName(request.getCafeName());

        hellRepository.save(originData);
    }

    public void delHell(long id) {
        hellRepository.deleteById(id);
    }
}
