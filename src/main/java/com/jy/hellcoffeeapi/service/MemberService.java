package com.jy.hellcoffeeapi.service;

import com.jy.hellcoffeeapi.entity.Member;
import com.jy.hellcoffeeapi.model.member.MemberCreateRequest;
import com.jy.hellcoffeeapi.model.member.MemberInfoChangeRequest;
import com.jy.hellcoffeeapi.model.member.MemberItem;
import com.jy.hellcoffeeapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;
    public Member getMember(long id) {
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request) {
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setBirthYear(request.getBirthYear());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setMemo(request.getMemo());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers() {
        List<Member> originList = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        for( Member member : originList ) {
            MemberItem addItem = new MemberItem();

            addItem.setId(member.getId());
            addItem.setName(member.getName());
            addItem.setBirthYear(member.getBirthYear());
            addItem.setPhoneNumber(member.getPhoneNumber());
            addItem.setMemo(member.getMemo());

            result.add(addItem);
        }

        return result;
    }

    public void putMember(long id, MemberInfoChangeRequest request) {
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setPhoneNumber(request.getPhoneNumber());
        originData.setMemo(request.getMemo());

        memberRepository.save(originData);
    }
}
