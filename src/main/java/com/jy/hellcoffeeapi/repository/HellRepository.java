package com.jy.hellcoffeeapi.repository;

import com.jy.hellcoffeeapi.entity.Hell;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HellRepository extends JpaRepository<Hell, Long> {
}
