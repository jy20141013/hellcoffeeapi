package com.jy.hellcoffeeapi.repository;

import com.jy.hellcoffeeapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long>{
}
