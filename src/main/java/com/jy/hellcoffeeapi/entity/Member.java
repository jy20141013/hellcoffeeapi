package com.jy.hellcoffeeapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private String birthYear;

    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;

    @Column(nullable = true, length = 100)
    private String memo;
}
