package com.jy.hellcoffeeapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Hell {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId")
    private Member member;

    @Column(nullable = false)
    private LocalDate dateHell;

    @Column(nullable = false)
    private Integer payPrice;

    @Column(nullable = false, length = 30)
    private String cafeName;

    @Column(nullable = false, length = 100)
    private String etcMemo;
}
