package com.jy.hellcoffeeapi.controller;

import com.jy.hellcoffeeapi.entity.Hell;
import com.jy.hellcoffeeapi.entity.Member;
import com.jy.hellcoffeeapi.model.hell.HellCreateRequest;
import com.jy.hellcoffeeapi.model.hell.HellItem;
import com.jy.hellcoffeeapi.model.hell.HellMemoChangeRequest;
import com.jy.hellcoffeeapi.repository.MemberRepository;
import com.jy.hellcoffeeapi.service.HellService;
import com.jy.hellcoffeeapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/hell")
public class HellController {
    private final HellService hellService;
    private final MemberService memberService;

    @PostMapping("/new/member-id/{memberId}")
    public String setHell(@PathVariable long memberId, HellCreateRequest request) {
        Member member = memberService.getMember(memberId);
        hellService.setHell(member, request);
        return "OK";
    }

    @GetMapping("/all")
    public List<HellItem> getHells() {
        return hellService.getHells();
    }

    @PutMapping("/memo-change/hell-id/{hellId}")
    public String putHell(@PathVariable long hellId, HellMemoChangeRequest request) {
        hellService.putHell(hellId, request);

        return "OK";
    }

    @DeleteMapping("/delete-hell/hell-id/{hellId}")
    public String delHell(@PathVariable long hellId) {
        hellService.delHell(hellId);

        return "OK";
    }
}
