package com.jy.hellcoffeeapi.controller;

import com.jy.hellcoffeeapi.model.member.MemberCreateRequest;
import com.jy.hellcoffeeapi.model.member.MemberInfoChangeRequest;
import com.jy.hellcoffeeapi.model.member.MemberItem;
import com.jy.hellcoffeeapi.service.MemberService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/new")
    public String setMember(@RequestBody MemberCreateRequest request) {
        memberService.setMember(request);
        return "OK";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers() {
        return memberService.getMembers();
    }

    @PutMapping("/info-change/member-id/{memberId}")
    public String putMember(@PathVariable long memberId, @RequestBody MemberInfoChangeRequest request) {
        memberService.putMember(memberId, request);
        return "OK";
    }
}
