package com.jy.hellcoffeeapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "지옥에서 온 커피 App",
                description = "돌림판을 돌려 커피 사주기 내기를 하자.",
                version = "v1"))
@RequiredArgsConstructor
@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi loveLineApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("커피내기 API v1")
                .pathsToMatch(paths)
                .build();
    }
}
